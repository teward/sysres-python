#!/usr/bin/env python3

#############################################
# NO USER SERVICEABLE PARTS IN THIS SCRIPT! #
#############################################

import psutil
import datetime
from email.mime.text import MIMEText
import email.utils
import smtplib
import socket
import sys
import time
import configparser
import os
import argparse

# DO NOT ALTER - this is a Python version check to make sure that we are using new enough Python!
if sys.version_info.major < 3 and sys.version_info.micro < 6:
    raise EnvironmentError("You can only use this script with Python 3.6 or newer!")

argparser = argparse.ArgumentParser()
argparser.add_argument('--config', '-c', nargs=1, optional=True, type=str, default=None)
args = argparser.parse_args()

if args.config:
    if os.path.exists(args.config):
        configpath = args.config
    else:
        raise FileNotFoundError("Unable to find specified config file.")
else:
    if os.path.exists(os.getcwd() + '/config'):
        configpath = os.getcwd() + '/config'
    else:
        if os.path.exists("/etc/sysres-python/config"):
            configpath = '/etc/sysres-python/config'
        else:
            raise FileNotFoundError("No config file available in current directory or in "
                                    "/etc/sysres-python/config. Aborting run.")

config = configparser.ConfigParser(allow_no_value=True)
config.read(configpath)

SMTP_SERVER = config.get('smtp', 'server')
SMTP_PORT = config.getint('smtp', 'port')
SMTP_SSL_REQUIRED = config.getboolean('smtp', 'ssl_required')
SMTP_AUTH_REQUIRED = config.getboolean('smtp', 'auth_required')
SMTP_USERNAME = config.get('smtp', 'username')
SMTP_PASSWORD = config.get('smtp', 'password')

# Adjust notification addresses for your needs.
NOTIFY_ADDRS = config.get('smtp', 'notify').split(',')
NOTIFY_FROM = config.get('smtp', 'from_addr')

# CPU tests
CPU_TEST = config.getboolean('cpu', 'enable_test')
CPU_USAGE_THRESHOLD = config.getint('cpu', 'usage_threshold')

# RAM Tests
RAM_TEST = config.getboolean('ram', 'enable_test')
RAM_USAGE_THRESHOLD = config.getint('ram', 'usage_threshold')

# SWAP Tests
SWAP_TEST = config.getboolean('swap', 'enable_test')
SWAP_USAGE_THRESHOLD = config.getint('swap', 'usage_threshold')


EMAIL_FORMAT = """{alarms}

System: {system}
Date/Time of Issue: {dt}
CPU Average: {cpu_perc}
RAM Usage: {ram_used} / {ram_avail}
Swap Usage: {swap_used} / {swap_avail}
"""


def _human_readable_size(size: int, decimal_places: int = 3) -> str:
    for unit in ['B', 'KiB', 'MiB', 'GiB', 'TiB']:
        if size < 1024.0:
            break
        size /= 1024.0
    return f"{size:.{decimal_places}f}{unit}"


def _send_email(alarms: str, cpu_perc: int, ram_used: int, ram_avail: int,
                swap_used:int, swap_avail: int) -> None:
    msg = MIMEText(EMAIL_FORMAT.format(
        alarms=alarms,
        system=socket.gethostname(),
        dt=datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
        cpu_perc=cpu_perc,
        ram_used=_human_readable_size(ram_used),
        ram_avail=_human_readable_size(ram_avail),
        ram_perc=int(float(ram_used / ram_avail) * 100),
        swap_used=_human_readable_size(swap_used),
        swap_avail=_human_readable_size(swap_avail),
        swap_perc=int(float(swap_used / swap_avail) * 100),
    ))

    msg['From'] = NOTIFY_FROM
    for addr in NOTIFY_ADDRS:
        msg['To'] = addr

    msg['Date'] = email.utils.format_datetime(datetime.datetime.utcnow())
    msg['Message-ID'] = email.utils.make_msgid()

    if SMTP_SSL_REQUIRED:
        smtp = smtplib.SMTP_SSL(SMTP_SERVER, SMTP_PORT)
    else:
        smtp = smtplib.SMTP(SMTP_SERVER, SMTP_PORT)
        try:
            smtp.starttls()
        except:
            smtp = smtplib.SMTP(SMTP_SERVER, SMTP_PORT)
        
    if SMTP_AUTH_REQUIRED:
        smtp.login(SMTP_USERNAME, SMTP_PASSWORD)
        
    smtp.sendmail(msg['From'], msg.get_all('To'), msg.as_bytes())
    

def _test_resources() -> None:
    time.sleep(0.5)  # Needed to get accurate CPU usage reading.
    cpu_perc = psutil.cpu_percent()
    ram_used = psutil.virtual_memory().used
    ram_avail = psutil.virtual_memory().total
    swap_used = psutil.swap_memory().used
    swap_avail = psutil.swap_memory().total

    # Alarms
    cpu_alarm = False
    ram_alarm = False
    swap_alarm = False

    alarmstr = ""

    # CPU threshold alarm when CPU Usage is over 70%
    if CPU_TEST and (cpu_perc >= CPU_USAGE_THRESHOLD):
        cpu_alarm = True
        alarmstr += f"CPU usage is above {CPU_USAGE_THRESHOLD}%!\n"

    # RAM usage on system is over 90% of available RAM
    if RAM_TEST and ((ram_used / ram_avail) * 100) >= RAM_USAGE_THRESHOLD:
        ram_alarm = True
        alarmstr += f"RAM consumption is over {RAM_USAGE_THRESHOLD}% of available RAM!\n"

    # Swap has to be existing on system, and total swap usage is over 50%
    if SWAP_TEST and swap_avail != 0 and ((swap_used / swap_avail) * 100) >= SWAP_USAGE_THRESHOLD:
        swap_alarm = True
        alarmstr += f"SWAP consumption is over {SWAP_USAGE_THRESHOLD}% of total Swap Space!"

    if cpu_alarm or ram_alarm or swap_alarm:
        _send_email(alarmstr, cpu_perc, ram_used, ram_avail, swap_used, swap_avail)


if __name__ == "__main__":
    _test_resources()
