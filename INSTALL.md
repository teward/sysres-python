INSTALLATION
============


## Prerequisite library installation 

Simply download the files to your system, and then install the `psutil` module 
from PyPI in your environment.

```python
pip3 install psutil
```

This requires you have PIP available in your environment. Once the `psutil` 
module is installed, you have all the prerequisites.


## Using `sysres.py`

Simply run `sysres.py` directly, or with `python3` on your system. It returns a preview of your system usage on the moment. Data are fetched by `psutil`. 

```
System: system.example.com
Date/Time: 2020-02-10 15:48:33
Load Avgs (1m, 5m, 15m): (1.65, 1.04, 1.08)
CPU Usage: 12.8
RAM Usage: 9.700GiB / 31.042GiB  (31%)
Swap Usage: 0.000B / 4.000GiB  (0%)
```


## Configure usage threshold and `sysres-email.py` email notification 

`sysres-email.py` is responsible for sending the email alert when your system usage has reached the predefined threashold. Before you deploy `sysres-email.py` you need to input necessary configurations in a `config` text file on the same directory as `sysres-python`. There's an example `config.example` template that you can edit, and save a copy as `config`.

```bash
# Example config template

[smtp]
server = smtp.example.com
port = 587
ssl_required = False
auth_required = True
username = username
password = password
notify = user@example.com,user2@example.com
from_addr = sysmonitor@system.example.com

[cpu]
enable_test = True
usage_threshold = 75

[ram]
enable_test = True
usage_threshold = 90

[swap]
enable_test = True
usage_threshold = 50
```
That's all for the email notification setup.


**Note:** It's up to you how you want to use `sysres-email.py` as a running service. Off-the-shelf the script would run well within `crontab`. 
