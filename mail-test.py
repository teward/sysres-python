#!/usr/bin/env python3

import argparse
import configparser
import datetime
import email.utils
from email.mime.text import MIMEText
import os
import smtplib
import sys

configpath = None

# DO NOT ALTER - this is a Python version check to make sure that we are using new enough Python!
if sys.version_info.major < 3 and sys.version_info.minor < 6:
    raise EnvironmentError("You can only use this script with Python 3.6 or newer!")

argparser = argparse.ArgumentParser()
argparser.add_argument('--config', '-c', nargs=1, required=False, type=str, default=None)
args = argparser.parse_args()

if args.config:
    if os.path.exists(args.config):
        configpath = args.config
    else:
        raise FileNotFoundError("Unable to find specified config file.")
else:
    if os.path.exists(os.getcwd() + '/config'):
        configpath = os.getcwd() + '/config'
    else:
        if os.path.exists("/etc/sysres-python/config"):
            configpath = '/etc/sysres-python/config'
        else:
            raise FileNotFoundError("No config file available in current directory or in "
                                    "/etc/sysres-python/config. Aborting run.")

config = configparser.ConfigParser(allow_no_value=True)
config.read(configpath)

SMTP_SERVER = config.get('smtp', 'server')
SMTP_PORT = config.getint('smtp', 'port')
SMTP_SSL_REQUIRED = config.getboolean('smtp', 'ssl_required')
SMTP_AUTH_REQUIRED = config.getboolean('smtp', 'auth_required')
SMTP_USERNAME = config.get('smtp', 'username')
SMTP_PASSWORD = config.get('smtp', 'password')

NOTIFY_ADDRS = config.get('smtp', 'notify').split(',')
NOTIFY_FROM = config.get('smtp', 'from_addr')


def _send_test_email() -> None:
    msg = MIMEText("This is a test message sent by sysres-python's email test script.")

    msg['From'] = NOTIFY_FROM
    for addr in NOTIFY_ADDRS:
        msg['To'] = addr

    msg['Date'] = email.utils.format_datetime(datetime.datetime.utcnow())

    if SMTP_SSL_REQUIRED:
        smtp = smtplib.SMTP_SSL(SMTP_SERVER, SMTP_PORT)
    else:
        smtp = smtplib.SMTP(SMTP_SERVER, SMTP_PORT)
        try:
            smtp.starttls()
        except smtplib.SMTPException:
            smtp = smtplib.SMTP(SMTP_SERVER, SMTP_PORT)

    if SMTP_AUTH_REQUIRED:
        smtp.login(SMTP_USERNAME, SMTP_PASSWORD)

    smtp.sendmail(msg['From'], msg.get_all('To'), msg.as_bytes())


if __name__ == "__main__":
    _send_test_email()
